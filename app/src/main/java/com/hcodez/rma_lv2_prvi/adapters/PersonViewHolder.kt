package com.hcodez.rma_lv2_prvi.adapters

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.hcodez.rma_lv2_prvi.PeopleRepository
import com.hcodez.rma_lv2_prvi.databinding.InspiringPersonItemBinding
import com.hcodez.rma_lv2_prvi.models.InspiringPerson
import com.hcodez.rma_lv2_prvi.utilities.getColorResource

class PersonViewHolder(itemView: View, private val context: Context) :
    RecyclerView.ViewHolder(itemView) {

    fun bind(person: InspiringPerson) {
        val itemBinding = InspiringPersonItemBinding.bind(itemView)
        itemBinding.tvScientistNameItem.text = person.name
        itemBinding.tvScientistBirthDateItem.text = person.birthDate
        itemBinding.tvScientistDescriptionItem.text = person.description
        itemBinding.imgScientistPictureItem.setBackgroundResource(
            getColorResource(person.image)
        )
        itemBinding.imgScientistPictureItem.setOnClickListener {
            if (PeopleRepository.getRandomQuoteFrom(person.name)?.isNotBlank() == true)
                Toast.makeText(
                    context,
                    PeopleRepository.getRandomQuoteFrom(person.name),
                    Toast.LENGTH_LONG
                ).show()
        }
    }
}