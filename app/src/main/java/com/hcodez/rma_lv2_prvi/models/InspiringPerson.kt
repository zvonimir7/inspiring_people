package com.hcodez.rma_lv2_prvi.models

import java.io.Serializable

data class InspiringPerson(
    val name: String,
    val image: String,
    val birthDate: String,
    val description: String
) : Serializable