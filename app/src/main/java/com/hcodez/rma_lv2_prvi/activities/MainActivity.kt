package com.hcodez.rma_lv2_prvi.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.hcodez.rma_lv2_prvi.PeopleRepository
import com.hcodez.rma_lv2_prvi.R
import com.hcodez.rma_lv2_prvi.adapters.InspiringPersonAdapter
import com.hcodez.rma_lv2_prvi.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityMainBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: InspiringPersonAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)

        initializeUI()
    }

    private fun initializeUI() {
        initializeRecyclerView()

        mainBinding.fabAddNewInspiringPerson.setOnClickListener {
            val newPersonIntent = Intent(this, NewPersonActivity::class.java)
            startActivity(newPersonIntent)
        }
    }

    private fun initializeRecyclerView() {
        linearLayoutManager = LinearLayoutManager(this)
        mainBinding.recyclerView.layoutManager = linearLayoutManager
        var scientists = PeopleRepository.getPeople()
        adapter = InspiringPersonAdapter(scientists)
        mainBinding.recyclerView.adapter = adapter
    }

    override fun onResume() {
        adapter.refreshData(PeopleRepository.getPeople())
        super.onResume()
    }
}